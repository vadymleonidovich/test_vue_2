import { createApp } from 'vue'
import App from './App.vue'

import Buefy from 'buefy';
//import 'bootstrap/dist/css/bootstrap.min.css';

const app = createApp(App)

app.use(Buefy);
app.mount('#app')
